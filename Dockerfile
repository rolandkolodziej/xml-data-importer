FROM php:7.4-cli-alpine

COPY . /usr/src/xml-data-importer
WORKDIR /usr/src/xml-data-importer

# intentionally disabled - allow to execute various commands from running container
#CMD ["php", "bin/console", "import"]

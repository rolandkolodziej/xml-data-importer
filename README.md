# Coding Task - XML Data Importer
Created in result of recruitment task for co.brick

## Installation (by cloning)
1. Clone this project to your local
```shell
git clone https://rolandkolodziej@bitbucket.org/rolandkolodziej/xml-data-importer.git
```
2. Install dependencies:
```shell
composer install
```
3. Build docker image:
```shell
docker build -t netborg/xml-data-importer .
```

## Installation (by pulling docker image)
1. Pull project image from docker hub.
```shell
docker pull netborg/xml-data-importer:latest
```

## Usage examples
### Execute `import` command (interactive)
1. Execute interactive `import` command to get prompted for source and destination DSNs: 
```shell
docker run -it --rm \
 -v $(pwd):/usr/src/xml-data-importer/file \
 --name xml-data-importer netborg/xml-data-importer php bin/console import
```

You will be asked for data source DSN. Please provide either:

- for local file data source:
```shell
file:///usr/src/xml-data-importer/var/coffee_feed.xml
```
- for remote file data source:
```shell
ftp://pupDev:pupDev2018@transport.productsup.io/coffee_feed.xml
```

Then you will be asked for CSV file destination DNS: 
```shell
file:///usr/src/xml-data-importer/file/output.csv
```

### Execute `import` command (non-interactive)
You can execute `import` command non-interactively by providing source and destination DSNs via env vars `DATA_SOURCE_DNS` and `DATA_DESTINATION_DNS`:
```shell
docker run -it --rm \
 -v $(pwd):/usr/src/xml-data-importer/file \
 -e "DATA_SOURCE_DNS=file:///usr/src/xml-data-importer/var/coffee_feed.xml" \
 -e "DATA_DESTINATION_DNS=file:///usr/src/xml-data-importer/file/output.csv" \
 --name xml-data-importer netborg/xml-data-importer php bin/console import
```

### Running tests
To run tests execute `php bin/phpunit` command or run docker container:
```shell
docker run -it --rm \
 -v $(pwd):/usr/src/xml-data-importer/file \
 --name xml-data-importer netborg/xml-data-importer php bin/phpunit
```

### Extras
- You can store imported data in different storage by providing optional parameter `storage` with supported storage type, for example:
```shell
bin/console import --storage=mysql
```
__Please note__ that currently the only supported storage is `csv` (default).

- You can add another data storage by adding another class implementing `\App\Interfaces\DataStorageInterface`:
```php
<?php
declare(strict_types=1);

namespace App\Storage;

use App\Interfaces\DataStorageInterface;

class MyCustomStorage implements DataStorageInterface
{
    /** @var string|null */
    protected $dsn;

    /**
     * @inheritDoc
     */
    public function setDsn(string $dsn): DataStorageInterface
    {
        $this->dsn = $dsn;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function supports(string $type): bool
    {
        return 'my_custom_data_storage_type' === $type;
    }

    /**
     * @inheritDoc
     */
    public function store(array $data): bool
    {
        // TODO: Implement your custom storage procedure.
    }
}
```
or simply by extending abstract class `\App\Storage\AbstractDataStorage`:
```php
<?php
declare(strict_types=1);

namespace App\Storage;

use App\Storage\AbstractDataStorage;

class MyCustomStorage extends AbstractDataStorage
{
    protected const TYPE = 'my_custom_data_storage_type';

    /**
     * @inheritDoc
     */
    public function store(array $data): bool
    {
        // TODO: Implement your custom storage procedure.
    }
}
```
and that's it ... your custom storage will be automatically installed inside Symfony's container and available for execution. To use your custom storage with `import` command - add an optional parameter with your custom storage type:

`bin/console import --storage=my_custom_data_storage_type`

- all error logs are being stored in default Symfony's log location `/var/log`.

<?php
declare(strict_types=1);

namespace App\Storage;

use App\DependencyInjection\Enum\DataStorageType;

class CsvFileStorage extends AbstractDataStorage
{
    protected const TYPE = DataStorageType::CSV_FILE_STORAGE;

    /**
     * @inheritDoc
     */
    public function store(array $data): bool
    {
        if (empty($data)) {
            $this->logger->info('No data to process.');
            return false;
        }

        $preProcessedData = $this->preProcessData($data);
        $this->ensureDirExists();

        if ($f = fopen($this->dsn, 'w')) {
            $error = false;
            foreach ($preProcessedData as $row) {
                if (false === fputcsv($f, $row)) {
                    $this->logger->error('Unable to save CSV row!');
                    $error = true;
                    break;
                }
            }
            fclose($f);
            return !$error;
        }

        $this->logger->error(sprintf('Unable to open file for writing `%s` due to: %s.', $this->dsn, error_get_last()['message']));
        return false;
    }

    protected function preProcessData(array $data): array
    {
        $headers = false;
        $preProcessed = [];
        foreach ($data as $row) {
            if (!$headers) {
                $preProcessed[] = array_keys($row);
                $headers = true;
            }
            $preProcessed[] = array_values($row);
        }

        return $preProcessed;
    }

    protected function ensureDirExists(): void
    {
        if (!file_exists($dir = dirname($this->dsn))) {
            mkdir($dir, 0774, true);
            chmod($dir, 0774);
        }
    }
}

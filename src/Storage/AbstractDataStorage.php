<?php
declare(strict_types=1);

namespace App\Storage;

use App\Interfaces\DataStorageInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractDataStorage implements DataStorageInterface
{
    protected const TYPE = null;

    /** @var LoggerInterface */
    protected $logger;

    /** @var string|null */
    protected $dsn;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function setDsn(string $dsn): DataStorageInterface
    {
        $this->dsn = $dsn;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function supports(string $type): bool
    {
        return static::TYPE === $type;
    }

    /**
     * @inheritDoc
     */
    abstract public function store(array $data): bool;
}

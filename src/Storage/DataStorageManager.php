<?php

declare(strict_types=1);

namespace App\Storage;

use App\Exception\UnsupportedDataStorageException;
use App\Interfaces\DataStorageInterface;

class DataStorageManager
{
    /** @var DataStorageInterface[] */
    protected static $dataStorage = [];

    public static function registerDataStorage(DataStorageInterface $dataStorage): void
    {
        self::$dataStorage[] = $dataStorage;
    }


    public function getDataStorage(string $type): DataStorageInterface
    {
        foreach (self::$dataStorage as $dataStorage) {
            if ($dataStorage->supports($type)) {
                return $dataStorage;
            }
        }

        throw new UnsupportedDataStorageException(sprintf('Unable to find data storage of type `%s`!', $type));
    }
}

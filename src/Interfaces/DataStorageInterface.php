<?php

namespace App\Interfaces;

interface DataStorageInterface
{
    /**
     * Sets Data Source Name (DSN).
     *
     * @param string $dsn
     * @return mixed
     */
    public function setDsn(string $dsn): DataStorageInterface;

    /**
     * Check if storage supports selected storage type.
     *
     * @param string $type
     * @return bool
     */
    public function supports(string $type): bool;

    /**
     * Store data in supported storage.
     *
     * @param array $data
     * @return bool
     */
    public function store(array $data): bool;
}

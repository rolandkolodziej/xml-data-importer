<?php

namespace App\Interfaces;

interface DataReaderInterface
{
    /**
     * Sets Data Source Name (DSN).
     *
     * @param string $dsn
     * @return mixed
     */
    public function setDsn(string $dsn): DataReaderInterface;

    /**
     * Read and return data.
     *
     * @return array
     */
    public function read(): array;

}

<?php
declare(strict_types=1);

namespace App\Exception;

class UnsupportedDataStorageException extends \RuntimeException
{

}

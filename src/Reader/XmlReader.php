<?php
declare(strict_types=1);

namespace App\Reader;

use App\Exception\UnreadableLocationException;
use App\Interfaces\DataReaderInterface;
use Psr\Log\LoggerInterface;

class XmlReader implements DataReaderInterface
{
    /** @var LoggerInterface */
    protected $logger;

    /** @var string|null */
    protected $dsn;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setDsn(string $dsn): DataReaderInterface
    {
        $this->dsn = $dsn;
        return $this;
    }

    /**
     * @throws UnreadableLocationException
     */
    public function read(): array
    {
        if (!$this->dsn || !$xmlString = file_get_contents($this->dsn)) {
            $msg = sprintf('Unable to read from location `%s`.', $this->dsn);
            $this->logger->critical($msg);
            throw new UnreadableLocationException($msg);
        }

        try {
            $collection = [];
            $xml = simplexml_load_string($xmlString);

            foreach ($xml as $simpleXMLElement) {
                $collection[] = $this->normalize($simpleXMLElement);
            }

            return $collection;
        } catch (\Exception $e) {
            $this->logger->critical(sprintf('Unable to parse XML content due to: `%s`', $e->getMessage()));
        }

        return [];
    }

    protected function normalize(\SimpleXMLElement $element): array
    {
        $parsed = [];
        foreach (get_object_vars($element) as $key => $value) {
            $parsed[$key] = str_replace(["\n", "\r", "\t"],'', (string)$value);
        };

        return $parsed;
    }
}

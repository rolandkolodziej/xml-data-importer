<?php

namespace App\Command;

use App\DependencyInjection\Enum\DataStorageType;
use App\Interfaces\DataReaderInterface;
use App\Storage\DataStorageManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCommand extends Command
{
    private const OPTION_STORAGE = 'storage';

    protected static $defaultName = 'import';
    protected static $defaultDescription = 'Import data from XML source.';

    /** @var string|null */
    protected $sourceDns;

    /** @var string|null */
    protected $destinationDns;

    /** @var DataReaderInterface */
    protected $dataReader;

    /** @var DataStorageManager */
    protected $dataStorageManager;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        DataReaderInterface $dataReader,
        DataStorageManager $dataStorageManager,
        ?string $sourceDns,
        ?string $destinationDns,
        string $name = null
    ) {
        $this->logger = $logger;
        $this->dataReader = $dataReader;
        $this->dataStorageManager = $dataStorageManager;
        $this->sourceDns = $sourceDns;
        $this->destinationDns = $destinationDns;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addOption(self::OPTION_STORAGE, null, InputOption::VALUE_REQUIRED, 'Select Data Storage type.', DataStorageType::CSV_FILE_STORAGE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->isInteractive()){
            while (!$this->sourceDns) {
                $this->sourceDns = $io->ask("Provide data SOURCE DNS:");
            }

            while (!$this->destinationDns) {
                $this->destinationDns = $io->ask("Provide data DESTINATION DNS:");
            }
        }

        if (!$this->sourceDns) {
            $this->logger->error('Unable to import data! Data source DNS not provided.');
            if (!$io->isQuiet()) {
                $io->error('Please provide Data Source DNS information.');
                return Command::INVALID;
            }
        }

        if (!$this->destinationDns) {
            $this->logger->error('Unable to import data! Data destination DNS not provided.');
            if (!$io->isQuiet()) {
                $io->error('Please provide Data destination DNS location.');
                return Command::INVALID;
            }
        }

        $data = $this->dataReader
            ->setDsn($this->sourceDns)
            ->read()
        ;

        if (
            $this->dataStorageManager->getDataStorage($input->getOption(self::OPTION_STORAGE))
            ->setDsn($this->destinationDns)
            ->store($data)
        ) {
            $msg = 'Data successfully imported!';
            $this->logger->info($msg);

            if (!$io->isQuiet()){
                $io->success($msg);
            }
        }


        return Command::SUCCESS;
    }
}

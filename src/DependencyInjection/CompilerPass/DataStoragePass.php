<?php
declare(strict_types=1);

namespace App\DependencyInjection\CompilerPass;

use App\Storage\DataStorageManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class DataStoragePass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(DataStorageManager::class)) {
            return;
        }

        $definition = $container->findDefinition(DataStorageManager::class);

        foreach ($container->findTaggedServiceIds('data_storage') as $id => $tags) {
            $definition->addMethodCall('registerDataStorage', [new Reference($id)]);
        }
    }
}

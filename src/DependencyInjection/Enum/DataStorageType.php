<?php
declare(strict_types=1);

namespace App\DependencyInjection\Enum;

class DataStorageType
{
    public const CSV_FILE_STORAGE = 'csv';
}

<?php
declare(strict_types=1);

namespace App\Tests\Unit;

use App\Exception\UnreadableLocationException;
use App\Reader\XmlReader;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;

class XmlReaderTest extends TestCase
{
    use ProphecyTrait;

    public function testUnableToReadLocationBehavior(): void
    {
        $logger = $this->prophesize(LoggerInterface::class);
        $logger
            ->critical(Argument::type('string'))
            ->shouldBeCalledOnce();

        $this->expectException(UnreadableLocationException::class);

        $service = new XmlReader($logger->reveal());

        $result = $service->read();

        $this->assertNull($result);
    }

    public function testInvalidXmlContentReadErrorBehavior(): void
    {
        $logger = $this->prophesize(LoggerInterface::class);
        $logger
            ->critical(Argument::type('string'))
            ->shouldBeCalledOnce();

        $dsn = __DIR__ . '/../resources/invalid.xml';

        $service = new XmlReader($logger->reveal());

        $result = $service
            ->setDsn($dsn)
            ->read();

        $this->assertIsArray($result);
        $this->assertSame([], $result);
    }

}

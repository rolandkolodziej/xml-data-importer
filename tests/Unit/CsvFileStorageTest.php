<?php
declare(strict_types=1);

namespace App\Tests\Unit;

use App\Storage\CsvFileStorage;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Log\LoggerInterface;

/** @covers \App\Storage\CsvFileStorage */
class CsvFileStorageTest extends TestCase
{
    use ProphecyTrait;

    public function testStoreEmptyData(): void
    {
        $data = [];

        $logger = $this->prophesize(LoggerInterface::class);
        $logger
            ->info(Argument::type('string'))
            ->shouldBeCalledOnce();

        $service = new CsvFileStorage($logger->reveal());

        $result = $service->store($data);

        $this->assertFalse($result);
    }
}

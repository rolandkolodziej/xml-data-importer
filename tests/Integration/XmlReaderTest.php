<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\Reader\XmlReader;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/** @covers \App\Reader\XmlReader */
class XmlReaderTest extends KernelTestCase
{
    /** @dataProvider readDataProvider */
    public function testRead(string $dsn): void
    {
        self::bootKernel();

        $logger = self::getContainer()->get(LoggerInterface::class);

        $expectedData = [
            ['column1' => 'Entry 1', 'column2' => '1', 'column3' => '1234567.0099', 'column4' => '0'],
            ['column1' => 'Entry 2', 'column2' => '0', 'column3' => '1.99', 'column4' => '24'],
            ['column1' => 'Entry 3', 'column2' => '10', 'column3' => '2331.239', 'column4' => '50'],
        ];

        $service = new XmlReader($logger);
        $result = $service
            ->setDsn($dsn)
            ->read();

        $this->assertIsArray($result);
        $this->assertSame($expectedData, $result);
    }

    public function readDataProvider(): \Generator
    {
        yield ['file:///usr/src/xml-data-importer/tests/resources/test.xml'];
        yield [ __DIR__ . '/../resources/test.xml' ];
    }
}

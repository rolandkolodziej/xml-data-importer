<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\Storage\CsvFileStorage;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CsvFileStorageTest extends KernelTestCase
{
    /** @dataProvider storeToFileDataProvider */
    public function testStoreToFile(array $data, array $expected): void
    {
        self::bootKernel();

        $logger = self::getContainer()->get(LoggerInterface::class);

        $dsn = __DIR__ . '/../resources/output.test.csv';
        $this->ensureDirExists($dsn);

        $service = new CsvFileStorage($logger);

        $result = $service
            ->setDsn($dsn)
            ->store($data);
        $check = $this->readFromFile($dsn);

        $this->assertTrue($result);
        $this->assertIsArray($check);
        $this->assertSame($expected, $check);
    }

    public function storeToFileDataProvider(): \Generator
    {
        yield [
            [
                [
                    'column1' => 'Entry 1',
                    'column2' => true,
                    'column3' => 1,
                    'column4' => 0.99,
                ],
                [
                    'column1' => 'Entry 2',
                    'column2' => false,
                    'column3' => 2,
                    'column4' => 12345678.0099,
                ],
            ],

            [
                ['column1', 'column2', 'column3', 'column4'],
                ['Entry 1', '1', '1', '0.99'],
                ['Entry 2', '', '2', '12345678.0099'],
            ]
        ];

        yield [
            [
                [
                    'column1' => 'Entry 1',
                    'column2' => 'true',
                    'column3' => '1',
                    'column4' => '0.99',
                ],
                [
                    'column1' => 'Entry 2',
                    'column2' => 'false',
                    'column3' => '2',
                    'column4' => '12345678.0099',
                ],
            ],

            [
                ['column1', 'column2', 'column3', 'column4'],
                ['Entry 1', 'true', '1', '0.99'],
                ['Entry 2', 'false', '2', '12345678.0099'],
            ]
        ];
    }

    private function ensureDirExists(string $dsn): void
    {
        if (!file_exists($dir = dirname($dsn))) {
            mkdir($dir, 0774, true);
            chmod($dir, 0774);
        }
    }

    private function readFromFile(string $dsn): array
    {
        $check = [];
        $f = fopen($dsn, 'r');
        while($row = fgetcsv($f)) {
            $check[] = $row;
        }
        fclose($f);
        unlink($dsn);

        return $check;
    }
}
